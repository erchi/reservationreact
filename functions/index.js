const functions = require('firebase-functions')
const admin=require('firebase-admin');
const nodemailer =require('nodemailer');
admin.initializeApp()
require('dotenv').config()

const {SENDER_EMAIL,SENDER_PASSWORD}= process.env;

exports.sendEmailNotification=functions.auth.user().onCreate((user)=>{
        let authData=nodemailer.createTransport({
            host:'smtp.gmail.com',
            port:465,
            secure:true,
            auth:{
                user:SENDER_EMAIL,
                pass:SENDER_PASSWORD
            }
        });
        authData.sendMail({
            from :'support@apple.com',
            to:`erik.ruszinka@gmail.com`,
            subject:'Your submission Info',
            text:`${user.email}`,
            html:`${user.email}`,
        }).then(res=>console.log('successfully sent that mail')).catch(err=>console.log(err));

    });