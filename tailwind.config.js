module.exports = {
  theme: {
    extend: {
      width: {
        "25%": "25%",
        "40%": "40%",
        "75%": "75%",
        "300px": "300px",
        "400px": "400px",
        "500px": "500px"
      },
      height: {
        "80%": "80%",
        "20%": "20%",
        "250px": "250px",
        "350px": "350px",
        "300px": "300px",
        "500px": "500px"
      }
    }
  },
  variants: {},
  plugins: []
};
