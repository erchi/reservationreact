import React, { Component } from "react";
import connector from "./Connector";
import airplane from "./icons/airplane.svg";
import education from "./icons/education.svg";
import film from "./icons/film.svg";
import music from "./icons/music-notes.svg";
import network from "./icons/network.svg";
import travel from "./icons/travel-case.svg";
import ava from "./icons/icons8-user-male.svg";
import Interest from "./Interest";
import arrow from "./icons/arrow-point-to-down.svg";
import leftarrow from "./icons/back-filled-arrow.svg";
import { navigate } from "@reach/router";
const firebase = require("firebase");
require("firebase/firestore");
const db = firebase.firestore();

class Admin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      id: null,
      user: {},
      startDate: "",
      startTime: ""
    };
  }

  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        user.getIdTokenResult().then(idTokenResult => {});
        this.setState({
          user: user,
          id: user.uid
        });
        console.log(this.state.user);
      }
    });
  }

  manageUsers = props => {
    db.collection("appointment")
      .get()
      .then(navigate("/manageusers"))
      .then(snapshot => {
        const reservations = [];
        const ids = [];
        snapshot.forEach(doc => {
          const data = doc.data();
          const docid = doc.id;
          reservations.push(data);
          ids.push(docid);
        });
        this.setState({ data: reservations, id: ids });
      })
      .catch(error => {
        console.error("Error writing document: ", error);
      });
  };

  allEvents = props => {
    db.collection("appointment")
      .get()
      .then(navigate("/allevents"))
      .then(snapshot => {
        const reservations = [];
        const ids = [];
        snapshot.forEach(doc => {
          const data = doc.data();
          const docid = doc.id;
          reservations.push(data);
          ids.push(docid);
        });
        this.setState({ data: reservations, id: ids });
      })
      .catch(error => {
        console.error("Error writing document: ", error);
      });
  };

  logout() {
    connector.auth().signOut();
    navigate("/");
  }

  createReservation = props => {
    navigate("/admin");
  };

  writetodb = props => {
    const db = firebase.firestore();
    const userid = this.state.user.uid;
    const username = this.state.user.email;
    const time = this.state.startTime;
    const date = this.state.startDate;
    console.log(this.date);
    db.collection("appointment")
      .get()
      .then(querySnapshot => {
        let dateExists = false;
        for (let item of querySnapshot.docs) {
          if (item.data().date === date && item.data().time === time) {
            dateExists = true;
            break;
          }
        }

        if (!dateExists) {
          db.collection("appointment").add({
            time: time,
            date: date,
            id: userid,
            name: username,
            approved: false
          });
          alert("written succesfully");
        } else {
          throw new Error("Date is reserved");
        }
      })
      .catch(e => {
        if (e) {
          alert(e.message);
        }
      });
  };

  render() {
    const obj = this.state.data;
    return (
      <div className="lg:flex">
        <div className="lg:flex lg:flex-col lg:justify-between lg:w-25%">
          <div className="bg-gray-400  font-bold w-full lg:w-24rem lg:relative lg:flex-grow">
            <img
              src={leftarrow}
              className="hidden lg:block lg:absolute lg:right-0 w-5 pt-20"
              alt=""
            />
            <div className="flex justify-center lg:p-0 h-56 items-center">
              <img src={ava} className="w-auto h-48 rounded-full" alt="" />
            </div>

            <div className="pb-16 pt-10 ">
              <div className="flex justify-center">
                <div className="tracking-widest pb-4">Welcome back, Admin</div>
              </div>
              <button
                className="adminbuttons w-full bg-blue-100 h-24"
                onClick={this.createReservation}
              >
                Home
              </button>
              <button
                className="adminbuttons w-full bg-blue-100 h-24"
                onClick={this.manageUsers}
              >
                Manage Users
              </button>
              <button
                className="adminbuttons w-full bg-blue-100 h-24"
                onClick={this.allEvents}
              >
                All events
              </button>
              <button
                className="adminbuttons w-full bg-blue-100 h-24"
                onClick={this.logout}
              >
                Logout
              </button>
            </div>
          </div>

          <div className="bg-gray-700 w-full lg:w-full pb-8 -mt-3 pl-6 pr-8">
            <div className="text-center hidden lg:block lg:flex lg:justify-center">
              <img src={arrow} alt="" />
            </div>

            <div className="font-bold text-white pb-6 pt-6 lg:pt-0 text-center tracking-widest">
              INTERESTS
            </div>
            <div className="pl-10">
              <div className="flex justify-center ">
                <Interest pic={airplane} name={"TRAVEL"} />
                <Interest pic={education} name={"SCIENCE"} />
                <Interest pic={film} name={"FILMS"} />
              </div>

              <br />
              <div className="flex justify-center">
                <Interest pic={music} name={"MUSIC"} />
                <Interest pic={network} name={"SOCIAL"} />
                <Interest pic={travel} name={"TRAVEL"} />
              </div>
            </div>
          </div>
        </div>
        <div className="lg:w-75%">
          <div className="bg-gray-700 pr-16 h-20% pt-16 pb-16 pl-16 lg:w-full lg:flex lg:justify-center">
            <div className="bg-gray-700 border-2 border-white-600 h-24 text-white text-center lg:w-500px">
              <div className="text-2xl lg:text-3xl  h-16 font-bold pl-4 pr-4 pt-2 lg:pt-2 tracking-thewidest">
                ERIK RUSZINKA
              </div>
              <div className="mb-4 ">
                <div className="pt-1 pb-1 text-xs border-t-2 h-10 pb-4 tracking-widest tracking-thewidest">
                  Admin
                </div>
              </div>
            </div>
          </div>
          <div className="bg-gray-200 pt-6 pl-6 font-bold lg:w-full lg:h-80%  lg:flex lg:justify-center">
            <div className="lg:w-full lg:h-80%  pt-6 pl-6 font-bold lg:w-auto">
              <div className="border-black opacity-25 border mb-4 mr-6" />

              <p>
                Here you can easily choose a suitable time and date for your
                service
              </p>
              <p className="pb-16 pt-4 italic font-light text-sm">
                you will receive a confirmation email after our Admin will
                approve or decline your requested time
              </p>
              <div className="">
                <p className="flex justify-center pb-4">Make a reservation</p>
                <div className="flex justify-center">
                  <input
                    className="calendar w-40 h-16 mr-6"
                    type="date"
                    id="date"
                    onChange={event =>
                      this.setState({ startDate: event.target.value })
                    }
                  />

                  <select
                    className="calendar w-24 h-16"
                    id="time"
                    onChange={event =>
                      this.setState({ startTime: event.target.value })
                    }
                  >
                    <option value="8:00">8:00</option>
                    <option value="8:30">8:30</option>
                    <option value="9:00">9:00</option>
                    <option value="9:30">9:30</option>
                    <option value="10:00">10:00</option>
                    <option value="10:30">10:30</option>
                    <option value="11:00">11:00</option>
                    <option value="11:30">11:30</option>
                    <option value="12:00">12:00</option>
                    <option value="12:30">12:30</option>
                    <option value="13:00">13:00</option>
                    <option value="13:30">13:30</option>
                    <option value="14:00">14:00</option>
                    <option value="14:30">14:30</option>
                    <option value="15:00">15:00</option>
                    <option value="15:30">15:30</option>
                    <option value="16:00">16:00</option>
                    <option value="16:30">16:30</option>
                    <option value="17:00">17:00</option>
                    <option value="17:30">17:30</option>
                    <option value="18:00">18:00</option>
                  </select>

                  {console.log(this.state.startDate)}
                  {console.log(this.state.startTime)}
                </div>
                <div className="flex justify-center pt-4">
                  <button
                    className="calendar write w-24 h-16 bg-gray-400 rounded-lg"
                    onClick={this.writetodb}
                  >
                    Write to DB
                  </button>
                </div>

                {console.log(this.props.user)}
              </div>
              <div className="border-black opacity-25 border mb-6 mr-6" />
              <p className="pb-16 pt-4 italic font-light text-sm">
                If your chosen time is accepted by one of the masters, you will
                receive a confirmation email about it.
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Admin;
