import React, { Component } from "react";
import connector from "./Connector";
import { Router } from "@reach/router";
import Home from "./Home";
import Admin from "./Admin";
import Login from "./Login";
import MyReservations from "./user/MyReservations";
import ManageUsers from "./admin/ManageUsers";
import AllEvents from "./admin/AllEvents";

class App extends Component {
  constructor(props) {
    super(props);
    this.app = connector;
    this.database = this.app;
  }

  render() {
    return (
      <Router>
        <Login path="/" />
        <Admin path="admin" />
        <MyReservations path="myreservations" />
        <ManageUsers path="manageusers" />
        <AllEvents path="allevents" />
        <Home path="home" />
      </Router>
    );
  }
}

export default App;
