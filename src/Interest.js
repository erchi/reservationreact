import React from "react";

const Interest = ({ pic, name }) => {
  return (
    <div className="w-12 mr-8 mb-5 flex flex-col items-center">
      <img src={pic} className="w-8 text-white pb-2"></img>
      <p className="text-white font-semibold text-sm text-center -ml-1">
        {name}
      </p>
    </div>
  );
};

export default Interest;
