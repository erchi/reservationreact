import React, { Component } from "react";
import connector from "../Connector";
import "../App.css";
import ava from "../icons/icons8-female-user.svg";
import Interest from "../Interest";
import airplane from "../icons/airplane.svg";
import education from "../icons/education.svg";
import film from "../icons/film.svg";
import music from "../icons/music-notes.svg";
import network from "../icons/network.svg";
import travel from "../icons/travel-case.svg";
import { navigate } from "@reach/router";
const firebase = require("firebase");
require("firebase/firestore");
const db = firebase.firestore();

class MyReservations extends Component {
  constructor(props) {
    super(props);
    this.app = connector;
    this.database = this.app;
    this.logout = this.logout.bind(this);

    this.state = {
      data: null,
      id: null,
      user: {},
      startDate: "",
      startTime: ""
    };
  }

  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        user.getIdTokenResult().then(idTokenResult => {});
        this.setState({
          user: user,
          id: user.uid
        });

        console.log(this.state.user);
      }
      this.myReservations();
    });
  }

  myReservations = props => {
    db.collection("appointment")
      .where("id", "==", this.state.user.uid)
      .get()
      .then(snapshot => {
        const reservations = [];
        const ids = [];
        snapshot.forEach(doc => {
          const data = doc.data();
          const docid = doc.id;
          reservations.push(data);
          ids.push(docid);
        });
        this.setState({ data: reservations, id: ids });
      })
      .catch(error => {
        console.error("Error writing document: ", error);
      });
  };

  createReservation = props => {
    navigate("/home");
  };

  logout() {
    connector.auth().signOut();
    navigate("/");
  }

  delete = id => {
    alert("Are you sure you want to delete this appointment?");
    db.collection("appointment")
      .doc(id)
      .delete()
      .then(() => {
        console.log("Document successfully deleted!");
      })
      .catch(error => {
        console.error("Error removing document: ", error);
      });
  };

  send() {
    var user = firebase.auth().currentUser;
    user
      .sendEmailVerification()
      .then(function() {
        console.log("Email sent");
      })
      .catch(function(error) {
        console.log("Error");
      });
  }

  render() {
    const obj = this.state.data;
    return (
      <div className="lg:flex">
        <div className="lg:flex lg:w-25% lg:flex-col lg:justify-between">
          <div className="bg-pic  font-bold w-full lg:w-24rem lg:relative lg:flex-grow">
            <div className="flex justify-center lg:p-0 h-56 items-center">
              <img src={ava} className="w-auto h-48 rounded-full" alt="" />
            </div>

            <div className="flex justify-center pb-10 pl-6 pt-6 mr-8">
              <div className="tracking-widest text-white">USER</div>
            </div>
            <button
              className="buttongroup w-full bg-blue-600 h-24"
              onClick={this.createReservation}
            >
              Home
            </button>
            <button
              className="buttongroup w-full bg-blue-600 h-24"
              onClick={this.myReservations}
            >
              My reservations
            </button>
            {this.state.user.emailVerified ? (
              <div></div>
            ) : (
              <button
                className="buttongroup w-full bg-blue-600 h-24"
                onClick={this.send}
              >
                Verification email
              </button>
            )}
            <button
              className="buttongroup w-full bg-blue-600 h-24"
              onClick={this.logout}
            >
              Logout
            </button>
          </div>

          <div className="bg-pic w-full lg:w-full pb-8 -mt-3 pl-6 pr-8">
            <div className="font-bold text-white pb-6 lg:mt-6 lg:pt-0 text-center tracking-widest">
              INTERESTS
            </div>
            <div className="pl-10">
              <div className="flex justify-center ">
                <Interest pic={airplane} name={"TRAVEL"} />
                <Interest pic={education} name={"SCIENCE"} />
                <Interest pic={film} name={"FILMS"} />
              </div>

              <br />
              <div className="flex justify-center">
                <Interest pic={music} name={"MUSIC"} />
                <Interest pic={network} name={"SOCIAL"} />
                <Interest pic={travel} name={"TRAVEL"} />
              </div>
            </div>
          </div>
        </div>
        <div className="lg:w-75%">
          <div className="bg-pic pr-16 h-20% pt-16 pb-16 pl-16 lg:w-full lg:flex lg:justify-center">
            <div className="bg-pic border-2 border-white-600 h-24 text-white text-center lg:w-500px">
              <div className="text-2xl lg:text-3xl  h-16 font-bold pl-4 pr-4 pt-2 lg:pt-2 tracking-thewidest">
                SALON
              </div>

              <div className="mb-4 ">
                <div className="pt-1 pb-1 text-xs border-t-2 h-10 pb-4 tracking-widest tracking-thewidest">
                  BARBERSHOP
                </div>
              </div>
            </div>
          </div>
          <div className="bg-gray-300 lg:h-80% pt-6 pl-6 pr-6 font-bold lg:w-auto">
            <div>
              <h1 className="pb-4">My Reservations</h1>
              <p>
                Here you can easily check which date was approved by the admin
              </p>
              <p className="pb-10 pt-4 italic font-light text-sm">
                you will receive a confirmation email after our Admin will
                approve or decline your requested time
              </p>
              {this.state.data &&
                this.state.data.map((reservation, i) => {
                  return (
                    <div className="pl-4 h-24 bg-gray-400 w-full lg:w-1/2 mb-4">
                      {console.log(reservation)}
                      <h5 key={i} className="pb-2 pt-4">
                        {reservation.time}
                      </h5>
                      <div className="flex justify-between w-full">
                        <p key={i} className="pr-3">
                          {reservation.name}
                        </p>
                        <p key={i} className="pr-3">
                          {reservation.date}
                        </p>
                        {reservation.approved ? (
                          <p
                            className="w-20 h-8 pt-1 pl-1 bg-green-100"
                            key={i}
                          >
                            Approved
                          </p>
                        ) : (
                          <p className="w-20 h-8 pt-1 pl-1 bg-yellow-100">
                            Pending
                          </p>
                        )}
                        <button
                          className="mr-4"
                          onClick={() => {
                            this.delete(this.state.id[i]);
                          }}
                        >
                          X
                        </button>
                      </div>
                    </div>
                  );
                })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default MyReservations;
