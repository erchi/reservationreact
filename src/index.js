import React from 'react';
import ReactDOM from 'react-dom';
import './App.css';
import './css/tailwind.css';
import App from './App';

ReactDOM.render(<App />, document.getElementById('root'));

