import React, { Component } from "react";
import connector from "./Connector";
import "./App.css";
import firebase from "firebase";
import { navigate } from "@reach/router";

class Login extends Component {
  constructor(props) {
    super(props);
    this.login = this.login.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.signup = this.signup.bind(this);
    this.state = {
      email: "",
      password: "",
      user: {}
    };
  }

  componentDidMount() {
    this.authListener();
  }

  authListener() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        user.getIdTokenResult().then(idTokenResult => {});
        this.setState({ user: user });
      } else {
        this.setState({ user: null });
      }
    });
  }

  login(e) {
    if (this.state.password === "" || this.state.email === "") {
      alert("field is empty");
    }
    e.preventDefault();
    connector
      .auth()
      .signInWithEmailAndPassword(this.state.email, this.state.password)
      .then(
        this.state.user ? (
          this.state.user.email === "erik.ruszinka@gmail.com" ? (
            navigate("/admin")
          ) : (
            navigate("/home")
          )
        ) : (
          <Login />
        )
      )
      .catch(error => {
        console.log(error);
      });
  }

  signup(e) {
    e.preventDefault();
    connector
      .auth()
      .createUserWithEmailAndPassword(this.state.email, this.state.password)
      .catch(error => {
        console.log(error);
      });
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  resetPassword = Email => {
    firebase
      .auth()
      .sendPasswordResetEmail(Email)
      .then(function(user) {
        console.log("Please check your email...");
      })
      .catch(function(e) {
        console.log(e);
      });
  };

  render() {
    return (
      <div className="bg-pic w-screen h-screen flex justify-center items-center font-sans">
        <div className="w-300px h-250px rounded-lg pr-4 pl-4 pb-5 flex-col justify-between bg-gray-200 shadow-lg">
          <p className="text-2xl text-center pb-2 pt-2">Login</p>
          <form>
            <input
              value={this.state.email}
              onChange={this.handleChange}
              type="email"
              id="email"
              required
              className="w-full h-10 mb-2 rounded-full calendar pl-4 bg-gray-400"
              name="email"
              placeholder="email"
            />

            <input
              value={this.state.password}
              onChange={this.handleChange}
              type="password"
              required
              id="password"
              className="w-full h-10 rounded-full pl-4 calendar bg-gray-400"
              name="password"
              placeholder="password"
            />
            <div className="flex justify-between mt-3">
              <div>
                <input
                  onClick={this.login}
                  type="submit"
                  className="adminbuttons text-lg rounded-lg h-10 w-24 calendar"
                  value="Log In"
                />
              </div>
              <div>
                <input
                  onClick={this.signup}
                  type="submit"
                  className="adminbuttons text-lg rounded-lg h-10 w-24 calendar"
                  value="Sign Up"
                />
              </div>
            </div>
          </form>
          <div className="flex justify-center mt-3">
            <a
              className="calendar"
              onClick={() => this.resetPassword(this.state.email)}
              href="#"
            >
              Forgot Password?
            </a>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
