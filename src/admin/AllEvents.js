import React, { Component } from "react";
import connector from "../Connector";
import airplane from "../icons/airplane.svg";
import education from "../icons/education.svg";
import film from "../icons/film.svg";
import music from "../icons/music-notes.svg";
import network from "../icons/network.svg";
import travel from "../icons/travel-case.svg";
import ava from "../icons/icons8-user-male.svg";
import Interest from "../Interest";
import arrow from "../icons/arrow-point-to-down.svg";
import leftarrow from "../icons/back-filled-arrow.svg";
import { navigate } from "@reach/router";
const firebase = require("firebase");
require("firebase/firestore");
const db = firebase.firestore();

class AllEvents extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      id: null,
      user: {}
    };
  }
  componentDidMount() {
    this.allEvents();
  }

  allEvents = props => {
    db.collection("appointment")
        .where("approved", "==", false)
        .get()
        .then(navigate("/allevents"))
        .then(snapshot => {
          const reservations = [];
          const ids = [];
          snapshot.forEach(doc => {
            const data = doc.data();
            const docid = doc.id;
            console.log(data);
            reservations.push(data);
            ids.push(docid);
          });
          this.setState({ data: reservations, id: ids });
        })
        .catch(error => {
          console.error("Error writing document: ", error);
        });
  };

  manageUsers = props => {
    db.collection("appointment")
      .get()
      .then(navigate("/manageusers"))
      .then(snapshot => {
        const reservations = [];
        const ids = [];
        snapshot.forEach(doc => {
          const data = doc.data();
          const docid = doc.id;
          reservations.push(data);
          ids.push(docid);
        });
        this.setState({ data: reservations, id: ids });
      })
      .catch(error => {
        console.error("Error writing document: ", error);
      });
  };

  logout() {
    connector.auth().signOut();
    navigate("/");
  }

  delete = id => {
    alert("Are you sure you want to delete this appointment?");
    db.collection("appointment")
      .doc(id)
      .delete()
      .then(() => {
        console.log("Document successfully deleted!");
        this.reload();
      })
      .catch(error => {
        console.error("Error removing document: ", error);
      });
  };

  createReservation = props => {
    navigate("/admin");
  };

  reload() {
    window.location.reload();
  }

  approve = id => {
    db.collection("appointment")
      .doc(id)
      .update({
        approved: true
      })
        .then(() => {
          console.log("Appointment approved successfully!");
          this.reload();
        })
        .catch(error => {
          console.error("Error removing document: ", error);
        });

  };

  render() {
    const obj = this.state.data;
    return (
      <div className="lg:flex">
        <div className="lg:flex lg:flex-col lg:justify-between lg:w-25%">
          <div className="bg-gray-400  font-bold w-full lg:w-24rem lg:relative lg:flex-grow">
            <img
              src={leftarrow}
              className="hidden lg:block lg:absolute lg:right-0 w-5 pt-20"
              alt=""
            />
            <div className="flex justify-center lg:p-0 h-56 items-center">
              <img src={ava} className="w-auto h-48 rounded-full" alt="" />
            </div>

            <div className="pb-16 pt-10">
              <div className="flex justify-center">
                <div className="tracking-widest pb-4">Welcome back, Admin</div>
              </div>
              <button
                  className="adminbuttons w-full bg-blue-100 h-24"
                  onClick={this.createReservation}
              >
                Home
              </button>
              <button
                className="adminbuttons w-full bg-blue-100 h-24"
                onClick={this.manageUsers}
              >
                Manage Users
              </button>
              <button
                className="adminbuttons w-full bg-blue-100 h-24"
                onClick={this.allEvents}
              >
                All events
              </button>
              <button
                className="adminbuttons w-full bg-blue-100 h-24"
                onClick={this.logout}
              >
                Logout
              </button>
            </div>
          </div>

          <div className="bg-gray-700 w-full lg:w-full pb-8 -mt-3 pl-6 pr-8">
            <div className="text-center hidden lg:block lg:flex lg:justify-center">
              <img src={arrow} alt="" />
            </div>

            <div className="font-bold text-white pb-6 pt-6 lg:pt-0 text-center tracking-widest">
              INTERESTS
            </div>
            <div className="pl-10">
              <div className="flex justify-center ">
                <Interest pic={airplane} name={"TRAVEL"} />
                <Interest pic={education} name={"SCIENCE"} />
                <Interest pic={film} name={"FILMS"} />
              </div>

              <br />
              <div className="flex justify-center">
                <Interest pic={music} name={"MUSIC"} />
                <Interest pic={network} name={"SOCIAL"} />
                <Interest pic={travel} name={"TRAVEL"} />
              </div>
            </div>
          </div>
        </div>
        <div className="lg:w-75%">
          <div className="bg-gray-700 pr-16 lg:h-20% h-56 pt-16 pb-16 pl-16 lg:w-full lg:flex lg:justify-center">
            <div className="bg-gray-700 border-2 border-white-600 h-24 text-white text-center lg:w-500px">
              <div className="text-2xl lg:text-3xl  h-16 font-bold pl-4 pr-4 pt-2 lg:pt-2 tracking-thewidest">
                ERIK RUSZINKA
              </div>
              <div className="mb-4 ">
                <div className="pt-1 pb-1 text-xs border-t-2 h-10 pb-4 tracking-widest tracking-thewidest">
                  Admin
                </div>
              </div>
            </div>
          </div>

          <div className="bg-gray-200 lg:w-full lg:h-80% pt-6 pl-6 font-bold lg:w-auto">
            <div className="border-black opacity-25 border mb-4 mr-6" />

            <div>
              <h1 className="mb-2">Upcoming events</h1>

              {this.state.data &&
                this.state.data.map((reservation, i) => {
                  return (
                    <div className="w-300px h-24 bg-gray-400 pl-2 mb-6 pt-1 rounded-lg">
                      <h5 className="pb-2" key={i}>
                        {reservation.time}
                      </h5>
                      <div className="flex">
                        <p key={i} className="pr-3 rounded-md">
                          {reservation.name}
                        </p>
                        <p key={i}>{reservation.date}</p>
                      </div>
                      <div className="flex justify-around pt-2">
                        <button
                          className="acceptbutton bg-green-200 w-20"
                          onClick={() => {
                            this.approve(this.state.id[i]);
                          }}
                        >
                          Approve
                        </button>
                        <button
                          className="declinebutton bg-red-200 w-20 rounded-md mr-2 "
                          onClick={() => {
                            this.delete(this.state.id[i]);
                          }}
                        >
                          Decline
                        </button>
                      </div>
                    </div>
                  );
                })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AllEvents;
