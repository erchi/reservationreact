import firebase from "firebase";

export const firebaseConfig = {
  apiKey: "AIzaSyDpAJemjTXEUMmPuN-davmKizY9TvIyzMo",
  authDomain: "reservation-global.firebaseapp.com",
  databaseURL: "https://reservation-global.firebaseio.com",
  projectId: "reservation-global",
  storageBucket: "reservation-global.appspot.com",
  messagingSenderId: "423607811860",
  appId: "1:423607811860:web:83a165ddb99410c54a0c01",
  measurementId: "G-YCVL9L01K5"
};

// Initialize Firebase
const connector = firebase.initializeApp(firebaseConfig);
firebase.analytics();

export default connector;
